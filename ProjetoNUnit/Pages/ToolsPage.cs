using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace ProjetoNUnit.Pages
{
    public class ToolsPage
    {
        private IWebDriver driverPage;
        private WebDriverWait wait;

        public ToolsPage(IWebDriver driver)
        {
            driverPage = driver;
            wait = new WebDriverWait(driverPage, TimeSpan.FromSeconds(5));
        }        

        public void Click(By elementoBy)
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(elementoBy));
            driverPage.FindElement(elementoBy).Click();
        }

        public bool Contain()
        {
            wait.Until(ExpectedConditions.ElementIsVisible(By.TagName("mat-row")));
            return driverPage.PageSource.Contains( "mat-cell");
        }
        public void ClicarMenuGerenciar()
        {
            Click(By.XPath("//button[@type='button'][text()='Gerenciar']"));

        }
        public void ClicarNaAbaProjetos()
        {
            Click(By.XPath("//*[@id='selectProject-div']/div[2]/div[3]"));
        }
        public void ClicarNoProjeto()
        {
            Click(By.CssSelector("mat-table > mat-row:nth-child(2) > mat-cell:nth-child(2)"));
        }
        public void ClicarNaAbaCasosDeTeste()
        {
            Click(By.Id("mat-tab-label-0-2"));
        }
        public void ClicarAceitarCookies()
        {
             Click(By.XPath("//*[@class='cc-btn cc-dismiss']"));

        }
    }
}