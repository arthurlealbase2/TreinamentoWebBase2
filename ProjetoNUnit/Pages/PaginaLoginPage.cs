using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace ProjetoNUnit.Pages
{
    public class LoginPage
    {
        private IWebDriver driverPage;
        private WebDriverWait wait;

        public LoginPage(IWebDriver driver)
        {
            driverPage = driver;
            wait = new WebDriverWait(driverPage, TimeSpan.FromSeconds(5));
        }        

        private IWebElement entradaEmail => driverPage.FindElement(By.Id("login"));

        private IWebElement entradaPassword => driverPage.FindElement(By.Id("password"));
        private IWebElement botaoLogin => driverPage.FindElement(By.XPath("//*[@class='btn btn-crowdtest btn-block']"));

        public void realizarLogin(string email, string senha)
        {
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("login")));
            entradaEmail.SendKeys(email);
            entradaPassword.SendKeys(senha);
            botaoLogin.Click();
        }
    }

    
}