using OpenQA.Selenium;
using NUnit.Framework;
using ProjetoNUnit.Pages;
using OpenQA.Selenium.Chrome;
using System;



namespace ProjetoNUnit.Tests
{
    class Tests
    {
        IWebDriver driver;
        
        [SetUp]
        public void startBrowser()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        }

        [Test]
        public void AcessarMeusCasosDeTeste()
        {
            driver.Navigate().GoToUrl("http://blackmirror.crowdtest.me.s3-website-us-east-1.amazonaws.com/auth");

            LoginPage loginPage = new LoginPage(driver);
            
            ToolsPage toolsPage = new ToolsPage(driver);
            toolsPage.ClicarAceitarCookies();

            loginPage.realizarLogin("arthur.leal@base2.com.br", "Facild+980");

            toolsPage.ClicarMenuGerenciar();
            toolsPage.ClicarNaAbaProjetos();
            toolsPage.ClicarNoProjeto();
            toolsPage.ClicarNaAbaCasosDeTeste();
            
            //Verifica se há algum teste criado
            Assert.IsTrue(toolsPage.Contain());
        }

        
    }
}