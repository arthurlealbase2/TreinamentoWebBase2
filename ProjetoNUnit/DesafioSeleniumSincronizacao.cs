using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Threading;

using System;

namespace ProjetoSeleniumSincronizacao
{
    [TestFixture]
    public class Desafios
    {
        [Test]
        public void NavegacaoDoSiteDropDownList()
        {
            IWebDriver driver = new ChromeDriver();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

            string url = "http://the-internet.herokuapp.com/dropdown";
            driver.Navigate().GoToUrl(url);

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("dropdown")));
            SelectElement combobox= new SelectElement(driver.FindElement(By.Id("dropdown")));
            combobox.SelectByValue("2");
            wait.Until(ExpectedConditions.ElementToBeSelected(By.XPath("//*[@id='dropdown']/option[3]")));

            Assert.AreEqual("2", driver.FindElement(By.Id("dropdown")).GetAttribute("value"));
            //driver.Quit();
            

        }

        [Test]
        public void AcessarMeusCasosDeTeste()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://blackmirror.crowdtest.me.s3-website-us-east-1.amazonaws.com/auth");
            driver.Manage().Window.Maximize();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("login")));
            driver.FindElement(By.Id("login")).SendKeys("email");

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("password")));
            driver.FindElement(By.Id("password")).SendKeys("senha");

            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@class='btn btn-crowdtest btn-block']")));
            driver.FindElement(By.XPath("//*[@class='btn btn-crowdtest btn-block']")).Click();

            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("/html/body/app-root/div[1]/app-landing/div/div[3]/div[1]/div[3]/div/button")));
            driver.FindElement(By.XPath("/html/body/app-root/div[1]/app-landing/div/div[3]/div[1]/div[3]/div/button")).Click();

            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("li-projects")));
            driver.FindElement(By.ClassName("li-projects")).Click();
            
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("/html/body/app-root/div[1]/app-projects/div/div/app-projects-list/div[4]/div[2]/div/mat-table/mat-row[2]/mat-cell[2]")));
            driver.FindElement(By.XPath("/html/body/app-root/div[1]/app-projects/div/div/app-projects-list/div[4]/div[2]/div/mat-table/mat-row[2]/mat-cell[2]")).Click();
            
            wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("mat-tab-label-0-2")));
            driver.FindElement(By.Id("mat-tab-label-0-2")).Click();

            wait.Until(ExpectedConditions.ElementIsVisible(By.TagName("mat-cell")));
            Assert.IsTrue(driver.FindElement(By.TagName("mat-cell")).Displayed);

            //driver.Quit();
        }
    }
}