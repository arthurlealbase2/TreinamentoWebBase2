using NUnit.Framework;

namespace ProjetoNUnit
{
    [TestFixture]
    public class Desafios
    {
        [Test]
        public void VerificarValorCarteira()
        {
            double saldo = 275.00;
            Assert.IsTrue(saldo.Equals(275.00));
        }

        [Test]
        public void VerificarNomeDiferente()
        {
            string nome = "arthur";
            Assert.IsFalse(nome.Equals("leal"));
        }
        [Test]
        public void VerificarSobrenome(){
            string sobrenome = "leal";
            Assert.AreEqual("leal", sobrenome);
        }
    }
}