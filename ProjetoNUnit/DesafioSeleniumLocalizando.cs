using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

using System;
namespace ProjetoSeleniumLocalizando
{
    [TestFixture]
    public class Desafios
    {
        [Test]
        public void VerificarResultadoPesquisa()
        {
            
            IWebDriver driver = new ChromeDriver();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(1));
            string url = "https://www.google.com/";
            driver.Navigate().GoToUrl(url);
            
            string pesquisa = "Google";

            driver.FindElement(By.ClassName("gLFyf")).SendKeys(pesquisa);
            driver.FindElement(By.ClassName("gLFyf")).SendKeys("\n");
            bool verificacaoResultado = driver.FindElement(By.XPath($".//*[starts-with(@class,'g tF2Cxc') and contains(text(),'{pesquisa}')]")).Displayed;
            Assert.IsTrue(verificacaoResultado);
            driver.Quit();


        }

        [Test]
        public void ValidarFormulárioComTodosOsCamposObrigatoriosPreenchidos()
        {
            IWebDriver driver = new ChromeDriver();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            string url = "https://ultimateqa.com/filling-out-forms/";
            
            driver.Navigate().GoToUrl(url);
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            string nome = "Arthur";
            string mensagem = "Teste";
            driver.FindElement(By.Id("et_pb_contact_name_0")).SendKeys(nome);
            driver.FindElement(By.Id("et_pb_contact_message_0")).SendKeys(mensagem);
            driver.FindElement(By.XPath("//*[@id='et_pb_contact_form_0']/div[2]/form/div/button")).Click();

            wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#et_pb_contact_form_0 > div > p")));

            Assert.AreEqual("Thanks for contacting us", driver.FindElement(By.CssSelector("#et_pb_contact_form_0 > div > p")).Text);

            //driver.Quit();
                       
            
        }

        [Test]
        public void ValidarFormulárioComUmDosCamposObrigatoriosPreenchidos()
        {
            IWebDriver driver = new ChromeDriver();
            
            string url = "https://ultimateqa.com/filling-out-forms/";
            driver.Navigate().GoToUrl(url);
        
            string nome = "Arthur";
            driver.FindElement(By.Id("et_pb_contact_name_0")).SendKeys(nome);
            driver.FindElement(By.XPath("//*[@id='et_pb_contact_form_0']/div[2]/form/div/button")).Click();

            //Verifica se a mensagem de erro de campos obrigatórios aparece
            Assert.AreEqual("Please, fill in the following fields:\r\nMessage", driver.FindElement(By.ClassName("et-pb-contact-message")).Text);
            
            driver.Quit();
                   
            
        }

        [Test]
        public void ValidarNavegaçãoLivre()
        {
            IWebDriver driver = new ChromeDriver();
            string url = "https://www.google.com/";
            driver.Navigate().GoToUrl(url);
            
            string pesquisa = "youtube";

            driver.FindElement(By.ClassName("gLFyf")).SendKeys(pesquisa);
            driver.FindElement(By.ClassName("gLFyf")).SendKeys("\n");

            driver.FindElement(By.CssSelector(".yuRUbf>a")).Click();

            driver.FindElement(By.Id("video-title-link")).Click();

            driver.Navigate().Refresh();
            driver.Navigate().Back();
            driver.Navigate().Back();

            driver.Navigate().Back();
            Assert.AreEqual("Google", driver.Title);
            driver.Quit();

        }
 
    }
}