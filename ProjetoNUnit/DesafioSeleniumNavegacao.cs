using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ProjetoSeleniumNavegacao
{
    [TestFixture]
    public class Desafios
    {
        [Test]
        public void ValidarNavegacao()
        {
            IWebDriver driver = new ChromeDriver();
            string url = "https://www.google.com/";
            driver.Navigate().GoToUrl(url);
            Assert.AreEqual("Google", driver.Title);
            driver.Quit();
        }

        [Test]
        public void VerificarTitulo()
        {
            IWebDriver driver = new ChromeDriver();
            string url = "https://www.google.com/";
            driver.Navigate().GoToUrl(url);            
            Assert.AreEqual("Google", driver.Title);
            driver.Quit();

        }

        [Test]
        public void VerificarUrlBrowser()
        {
            IWebDriver driver = new ChromeDriver();
            string url = "https://www.google.com/";
            driver.Navigate().GoToUrl(url);            
            Assert.AreEqual(url, driver.Url);
            driver.Quit();

        }
    }
}